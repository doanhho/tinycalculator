package com.calculator.doanh.tinycalculator;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    Button btAbout;
    TextView tvResult;
    TextView tvExpression;
    Button btClear;
    Button btDelete;
    Button btCalculate;
    Boolean isJustCal =  true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btAbout = (Button) findViewById(R.id.btAbout);
        btClear = (Button) findViewById(R.id.btClear);
        btDelete = (Button) findViewById(R.id.btDelete);
        btCalculate = (Button) findViewById(R.id.btCalculate);
        tvResult = (TextView) findViewById(R.id.tvResult);
        tvExpression = (TextView) findViewById(R.id.tvExpression);
        ResetUI();

        btAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Tiny Calculator by DoanhHN1@fsoft.com.vn", Toast.LENGTH_LONG).show();
            }
        });

        btClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetUI();
            }
        });

        btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = tvExpression.getText().toString();
                if (text.length()>1)
                {
                    tvExpression.setText(text.substring(0,text.length()-1));
                }
                else {
                    tvExpression.setText("0");
                    isJustCal = true;
                }
            }
        });

        btCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calculate();
            }
        });
    }



    public void OnClickForInput(View v)
    {
        if (isJustCal) {
            tvExpression.setText("0");
            isJustCal = false;
        }

        Button buttonInp = (Button) v;
        String expression = tvExpression.getText().toString();

        char lastChar = expression.charAt(expression.length() - 1);
        switch (buttonInp.getText().toString())
        {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                if (expression.equals("0")){
                    expression = buttonInp.getText().toString();
                }
                else {
                    expression += buttonInp.getText().toString();
                }
                break;
            case "+":
            case "*":
            case "/":
                if (lastChar == '+' || lastChar=='*' || lastChar == '/' || lastChar=='.')
                {
                    expression = expression.substring(0,expression.length()-1) + buttonInp.getText().toString();
                }
                else if (lastChar == '-')
                {
                    if (expression.charAt(expression.length() - 2) == '*' || expression.charAt(expression.length() - 2) == '/' )
                    {
                        expression = expression.substring(0,expression.length()-2) + buttonInp.getText().toString();
                    }
                    else
                    {
                        expression = expression.substring(0,expression.length()-1) + buttonInp.getText().toString();
                    }
                }
                else
                {
                    expression += buttonInp.getText().toString();
                }
                break;
            case "-":
                if (lastChar == '+' || lastChar == '-' || lastChar=='.')
                {
                    expression = expression.substring(0,expression.length()-1) + buttonInp.getText().toString();
                }
                else
                {
                    expression += buttonInp.getText().toString();
                }
                break;
            case ".":
                //check if has "." character before
                int j = expression.length();
                String operator = "+-*/";
                Boolean found = false;
                while (j>0 && !found && !operator.contains(String.valueOf(expression.charAt(j-1))))
                {
                    if (expression.charAt(j-1) == '.')
                    {
                        found = true;
                    }
                    j--;
                }

                if (!found)
                {
                    if (lastChar == '+' || lastChar == '-' || lastChar=='*' || lastChar == '/')
                    {
                        expression += "0.";
                    }
                    else
                    {
                        expression += ".";
                    }
                }
                break;
        }

        tvExpression.setText(expression);
    }

    private void Calculate()
    {
        AnalyseInputString();
        isJustCal = true;
    }

    private void AnalyseInputString()
    {
        String numberArr = "0123456789";

        String expression = tvExpression.getText().toString();
        int j = tvExpression.length();

        while (!numberArr.contains(String.valueOf(expression.charAt(j-1))))
        {
            expression = expression.substring(0,j-1);
            j--;
        }

        int start = 0;
        int end = expression.length();
        String operator = "+-*/";

        ArrayList<String> values = new ArrayList<String>();

        for (int index = 0; index<expression.length(); index ++)
        {
            if (operator.contains(String.valueOf(expression.charAt(index))))
            {
                if (expression.charAt(index) == '-')
                {
                    //not a negative sign
                    if (index>=1 && expression.charAt(index-1) != '*' && expression.charAt(index-1)!= '/')
                    {
                        String val = expression.substring(start, index);
                        values.add(val);
                        values.add(String.valueOf(expression.charAt(index)));
                        start = index +1;
                    }
                }
                else
                {
                    String val = expression.substring(start, index);
                    values.add(val);
                    values.add(String.valueOf(expression.charAt(index)));
                    start = index +1;
                }
            }
        }

        values.add(expression.substring(start, expression.length()));

        ArrayList<Double> operands = new ArrayList<Double>();
        ArrayList<String> operators = new ArrayList<String>();

        values.add("");
        for (int i=0; i<values.size(); i+=2) {
            operands.add(Double.parseDouble(values.get(i)));
            operators.add(values.get(i+1));
        }

        Boolean found = false;

        //* and /

        int i=0;
        while (i<operators.size()-1)
        {
            if (operators.get(i).equals("*") || operators.get(i).equals("/"))
            {
                Double val = Calculate(operands.get(i),operators.get(i),operands.get(i+1));
                operands.remove(i);
                operands.remove(i);
                operands.add(i,val);
                operators.remove(i);
                i = 0;
            }
            else {
                i++;
            }
        }

        i=0;
        while (i<operators.size()-1)
        {
            if (operators.get(i).equals("+") || operators.get(i).equals("-"))
            {
                Double val = Calculate(operands.get(i),operators.get(i),operands.get(i+1));
                operands.remove(i);
                operands.remove(i);
                operands.add(i,val);
                operators.remove(i);
                i = 0;
            }
            else {
                i++;
            }
        }

        Integer intResult = operands.get(0).intValue();
        if (intResult.doubleValue() == operands.get(0))
        {
            tvResult.setText(intResult.toString());
        }
        else
        {
            tvResult.setText(operands.get(0).toString());
        }

    }

    private final String POSINFINITY = "+INFINITY";
    private final String NEGINFINITY = "-INFINITY";

    private Double Calculate(Double a, String operator, Double b)
    {
        Double result = 0.0;
        switch (operator)
        {
            case "+":
                if (a == Double.POSITIVE_INFINITY) {
                    if (b == Double.NEGATIVE_INFINITY) {
                        result = 0.0;
                    } else {
                        result = Double.POSITIVE_INFINITY;
                    }
                }
                else if (a==Double.NEGATIVE_INFINITY)
                {
                    if (b==Double.POSITIVE_INFINITY)
                    {
                        result = 0.0;
                    }
                    else
                    {
                        result = Double.NEGATIVE_INFINITY;
                    }
                }
                else
                {
                    if (b == Double.POSITIVE_INFINITY || b==Double.NEGATIVE_INFINITY)
                    {
                        result = b;
                    }
                    else
                    {
                        result = a + b;
                    }
                }
                break;
            case "-":
                if (a == Double.POSITIVE_INFINITY) {
                    if (b == Double.POSITIVE_INFINITY) {
                        result = 0.0;
                    } else {
                        result = Double.POSITIVE_INFINITY;
                    }
                }
                else if (a==Double.NEGATIVE_INFINITY)
                {
                    if (b==Double.NEGATIVE_INFINITY)
                    {
                        result = 0.0;
                    }
                    else
                    {
                        result = Double.NEGATIVE_INFINITY;
                    }
                }
                else
                {
                    if (b == Double.POSITIVE_INFINITY || b==Double.NEGATIVE_INFINITY)
                    {
                        result = -b;
                    }
                    else
                    {
                        result = a - b;
                    }
                }
                break;
            case "*":
                if (a == 0 || b==0)
                {
                    result =0.0;
                }
                else
                {
                    result = a *b;
                }
                break;
            case "/":
                result = a/b;
                break;
        }

        return result;
    }

    private void ResetUI()
    {
        tvResult.setText("");
        tvExpression.setText("0");
        isJustCal = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
